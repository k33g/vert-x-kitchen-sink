import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import arrow.core.*
import garden.bots.*

{ router: Router ->

  router.get("/yo/:who") { context ->
    context.json(json {obj(
      "message" to "👋 Yo ${context.param("who")} 😍",
      "author" to "@k33g_org"
    )})
  }

  router.get("/oups") { context ->
    giveMeSomething().let {
      when(it) {
        is Failure -> context.json(json { obj("message" to it.exception.message) })
        is Success -> context.json(json { obj("number" to it.value) })
      }
    }
  }

  router.get("/trooper").handler { context ->
    context.json(JsonObject.mapFrom(
      Trooper(
        id= java.util.UUID.randomUUID().toString(),
        name= "Boba Fett",
        nickName= "Bob",
        age= (25..50).shuffled().first()
      )
    ))
  }

  router.get("/hello") { context ->
    context.json(json {obj(
      "message" to helloWorld()
    )})
  }

}

# Vert-x Kitchen Sink

This project is "experimental". I use it to test "things" with Vert-x and Kotlin.

Right now, it's an http server that loads and compiles dynamically Kotlin source code at start and when any change during the execution.

> only in these directories:
> - `modules`
> - `routes`
> - `plugins` will be used to load jar files dynamically (this a 🚧 WIP)

Your project structure should looks like that:

```
.

├── kitchen-sink-server.jar
├── modules
│   └── various.kt
├── plugins
│   └── README.md
├── public
│   └── index.html
├── routes
│   └── routes.kt

```

## Start

```
export PORT=9090
export STATIC="./public-vue"
java -jar ./kitchen-sink-server.jar
```

> where `STATIC` is the path to the web static assets

> **remark**: I use it to launch several http servers (on different htt ports) and "map" to various assets directories (useful for testing)

## Re build

```
mvn clean package
```


// a very bad function
import arrow.core.*

fun giveMeSomething(): Try<Any> {
  return Try {
    when((0..2).shuffled().first()) {
      0 -> throw Exception("🤭 Oups! I did it again")
      1 -> 42
      else -> 666
    }
  }
}

fun helloWorld(): String {
  return "👋 Hello 🌍 World 😍"
}
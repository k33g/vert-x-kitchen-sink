/**
 * Darth Vader Quotes
 */
class DarthVader {

  private var quotes: MutableList<String> = listOf(
    "Perhaps I can find new ways to motivate them.",
    "The Emperor will show you the true nature of the Force. He is your Master now.",
    "He will join us or die, Master."
  ).toMutableList()

  fun addQuote(quote: String) {
    quotes.add(quote)
  }

  fun getQuote(): String {
    return quotes.shuffled().first()
  }

}

data class Trooper(
  val id: String,
  val name: String,
  val nickName: String,
  val age: Int
)


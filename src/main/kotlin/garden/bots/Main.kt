package garden.bots

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import garden.bots.core.*
import io.vertx.core.AbstractVerticle
import io.vertx.core.Future
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.kotlin.core.http.HttpServerOptions

class Main : AbstractVerticle() {

  override fun start(startFuture: Future<Void>) {

    val kompilo = Kompilo()

    println("=============================================================================")
    println(" > loading plugins and modules")
    println("=============================================================================")

    // --- Load plugins (jar files) ---
    // 🚧 WIP
    loadPluginsFromDisk(
      pluginsPath= "${System.getProperty("user.dir")}/plugins",
      kompilo= kompilo
    )

    // --- Load and compile modules ---
    loadModulesFromDisk(
      modulesPath= "${System.getProperty("user.dir")}/modules",
      kompilo= kompilo
    )

    val envHttpPort = Option.fromNullable(System.getenv("PORT"))

    val httpPort: Int = when(envHttpPort) {
      is None -> 8080
      is Some -> {
        Integer.parseInt(envHttpPort.t)
      }
    }

    val router = Router.router(vertx)
    router.route().handler(BodyHandler.create())

    loadRoutesFromDisk(
      routesPath = "${System.getProperty("user.dir")}/routes",
      kompilo = kompilo,
      router = router
    )

    Sentinel(
      routesPath = "${System.getProperty("user.dir")}/routes",
      modulesPath = "${System.getProperty("user.dir")}/modules",
      kompilo= kompilo,
      router= router
    )

    /* === Start the server === */
    vertx.createHttpServer(HttpServerOptions(port = httpPort))
      .requestHandler(router)
      .listen { ar -> when {
        ar.failed() -> {
          startFuture.fail(ar.cause())
          println("😡 Houston? ${ar.cause().message}")
        }
        ar.succeeded() -> {
          startFuture.complete()
          println("😃 🌍 Main started on $httpPort")
        }
      }}
  }

}
package garden.bots

import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext

/**
 * Extensions functions
 */
fun Router.get(uri: String, handler: (RoutingContext) -> Unit ) {
  this.get(uri).handler(handler)
}

fun Router.post(uri: String, handler: (RoutingContext) -> Unit ) {
  this.post(uri).handler(handler)
}

fun Router.delete(uri: String, handler: (RoutingContext) -> Unit ) {
  this.delete(uri).handler(handler)
}

fun Router.put(uri: String, handler: (RoutingContext) -> Unit ) {
  this.put(uri).handler(handler)
}

fun RoutingContext.json(jsonObject: JsonObject) {
  this.response().putHeader("content-type", "application/json;charset=UTF-8").end(jsonObject.encodePrettily())
}

fun RoutingContext.text(content: String) {
  this.response().putHeader("content-type", "text/plain;charset=utf-8").end(content)
}

fun RoutingContext.html(content: String) {
  this.response().putHeader("content-type", "text/html; charset=utf-8").end(content)
}

fun RoutingContext.param(paramName: String): Any {
  return this.request().params().get(paramName)
}
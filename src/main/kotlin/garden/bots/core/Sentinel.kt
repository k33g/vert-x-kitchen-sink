package garden.bots.core


import io.vertx.ext.web.Router
import org.apache.commons.io.monitor.FileAlterationListener
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor
import org.apache.commons.io.monitor.FileAlterationMonitor
import org.apache.commons.io.monitor.FileAlterationObserver
import java.io.File

class Sentinel(routesPath: String, modulesPath: String, kompilo: Kompilo, router: Router) {
  // observe the changes
  private var routesObserver = FileAlterationObserver(routesPath)
  private var routesMonitor = FileAlterationMonitor(5*1000)

  private var routesListener: FileAlterationListener = object : FileAlterationListenerAdaptor() {
    override fun onFileCreate(file: File?) {
      println("-> routes creation: ${file?.nameWithoutExtension}")

      loadRoutesFromDisk(
        routesPath = "${System.getProperty("user.dir")}/routes",
        kompilo = kompilo,
        router = router
      )
    }

    override fun onFileDelete(file: File?) {
      //TODO 🚧 Work in progress
    }

    override fun onFileChange(file: File?) {
      println("-> routes change: ${file?.nameWithoutExtension}")

      loadRoutesFromDisk(
        routesPath = "${System.getProperty("user.dir")}/routes",
        kompilo = kompilo,
        router = router
      )
    }
  }

  // observe the changes
  private var modulesObserver = FileAlterationObserver(modulesPath)
  private var modulesMonitor = FileAlterationMonitor(5*1000)

  private var modulesListener: FileAlterationListener = object : FileAlterationListenerAdaptor() {

    override fun onFileCreate(file: File?) {
      println("-> modules creation: ${file?.nameWithoutExtension}")

      loadModulesFromDisk(
        modulesPath= "${System.getProperty("user.dir")}/modules",
        kompilo= kompilo
      )
    }

    override fun onFileDelete(file: File?) {
      //TODO 🚧 Work in progress
    }

    override fun onFileChange(file: File?) {

      println("-> modules change: ${file?.nameWithoutExtension}")

      loadModulesFromDisk(
        modulesPath= "${System.getProperty("user.dir")}/modules",
        kompilo= kompilo
      )
    }

  }

  init {
    println("🤖 > initializing the sentinel ...")

    routesObserver.addListener(routesListener);
    routesMonitor.addObserver(routesObserver);
    routesMonitor.start();

    modulesObserver.addListener(modulesListener);
    modulesMonitor.addObserver(modulesObserver);
    modulesMonitor.start();

  }

}
package garden.bots.core

import arrow.core.*
import garden.bots.core.Kompilo
import java.io.File

fun loadPluginsFromDisk(pluginsPath: String, kompilo: Kompilo) {
  // --- load plugins (jar files) ---

  Option.fromNullable(File(pluginsPath).listFiles()).let {
    when(it) {
      is None -> println("  👋 > no funktion plugin")
      is Some -> {
        val plugins = it.t
        plugins.filter { item -> item.extension =="jar" }.forEach { it ->
          val plugin = it

          Try { kompilo.addJar(plugin.canonicalPath) }.let { it ->
            when(it) {
              is Failure -> {
                println("  😡 > when loading ${plugin.canonicalPath}")
                println("  😡 > ------------------------------------------------------------------------------")
                println("  😡 > ${it.exception.stackTrace}")
                println("  😡 > ------------------------------------------------------------------------------")
              }
              is Success -> println("  🙂 > ${plugin.nameWithoutExtension} loaded from ${plugin.canonicalPath}")
            }
          }
        } // End Of File
      }
    }
  }
}
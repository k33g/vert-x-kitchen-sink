package garden.bots.core

import java.net.MalformedURLException
import java.net.URL
import java.net.URLClassLoader

class JarFileLoader(urls: Array<URL>) : URLClassLoader(urls) {

  @Throws(MalformedURLException::class)
  fun addFile(path: String) {
    val urlPath = "jar:file://$path!/"
    addURL(URL(urlPath))
  }
}

package garden.bots.core

import arrow.core.*
import java.io.File

fun loadModulesFromDisk(modulesPath: String, kompilo: Kompilo) {
  // --- Load and compile of commons modules

  Option.fromNullable(File(modulesPath).listFiles()).let {
    when(it) {
      is None -> println("  👋 > no funktion module")
      is Some -> {
        val modules = it.t
        modules.filter { item -> item.extension == "kt" }.forEach { file ->

          Try {

            val moduleName = file.name
            val path = file.canonicalPath.toString()
            val sourceCode = File(path).readText(Charsets.UTF_8)

            /* module compilation */
            println("  🤖 > Compiling module: $moduleName")

            kompilo.compileModule(sourceCode).let { it ->
              when(it) {
                is Failure -> {
                  println("  😡 > when compiling: ${it.exception.message}")
                  it.exception.printStackTrace()
                }
                is Success<*> -> println("  🙂 > $moduleName 👍")
              }
            }

          }.let { it ->
            when(it) {
              is Failure -> println("  😡 >  ${it.exception.message}")
              is Success -> {}
            }
          }

        } // End Of File
      }
    }
  }


}
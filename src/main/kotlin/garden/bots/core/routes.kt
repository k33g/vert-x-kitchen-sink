package garden.bots.core

import arrow.core.*
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.StaticHandler
import java.io.File

fun loadRoutesFromDisk(routesPath: String, kompilo: Kompilo, router: Router) {

  router.clear()
  val staticPath = Option.fromNullable(System.getenv("STATIC")).let {
    when(it) {
      is None -> "./public"
      is Some -> it.t
    }
  }
  val staticHandler = StaticHandler.create().setWebRoot(staticPath).setCachingEnabled(false)
  //router.route("/*").handler(StaticHandler.create())
  router.route("/*").handler(staticHandler)

  Option.fromNullable(File(routesPath).listFiles()).let {
    when(it) {
      is None -> println("  👋 > no routes module")
      is Some -> {
        val modules = it.t
        modules.filter { item -> item.extension == "kt" }.forEach { file ->

          Try {

            val moduleName = file.name
            val path = file.canonicalPath.toString()
            val sourceCode = File(path).readText(Charsets.UTF_8)

            /* module compilation */
            println("  🤖 > Compiling routes module: $moduleName")

            kompilo.compileAndExecuteRoutes(sourceCode, router).let { it ->
              when(it) {
                is Failure -> {
                  println("  😡🥶 > when compiling routes: ${it.exception.message}")
                  it.exception.printStackTrace()
                }
                is Success<*> -> println("  🙂 > $moduleName 👍")
              }
            }

          }.let { it ->
            when(it) {
              is Failure -> println("  😡 >  ${it.exception.message}")
              is Success -> {}
            }
          }

        } // End Of File
      }
    }
  }


}
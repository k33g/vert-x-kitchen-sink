package garden.bots.core

import arrow.core.Try
import io.vertx.ext.web.Router
import java.net.URL
import javax.script.ScriptEngineManager


//@Suppress("UNCHECKED_CAST")
class Kompilo {
  companion object {
    private val classLoader = JarFileLoader(arrayOf<URL>())
    private val engine = ScriptEngineManager().getEngineByExtension("kts")
  }

  fun addJar(jarPathFile: String) {
    val res = classLoader.addFile(jarPathFile)
  }

  fun loadClass(className: String): Class<*> {
    return classLoader.loadClass(className)
  }

  fun compileModule(sourceCode: String): Any {
    return Try { engine.eval(sourceCode) }
  }

  fun compileAndExecuteRoutes(sourceCode: String, router: Router): Try<Any>{
    return Try {
      (engine.eval(sourceCode) as (router: Router) -> Unit).invoke(router)
    }
  }
}